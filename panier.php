<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
    <link rel="stylesheet" href="cnc.css">
</head>
<body>
    <div id="liste">
    <?php
        require('pdo.php');

        $req = $pdo->query('select * from produit;');
        $mesInfos = $req->fetchAll();

        foreach($mesInfos as $data){
    ?>

        <div class="produit">
            <img src="<?= $data['image'] ?>" alt="<?= $data['nom'] ?>">
            <div>
                <h2><?= $data['nom'] ?></h2>
                <p><span><?= $data['prix'] ?></span>€</p>
            </div>
            <div>
                <form action="ajouter.php" method="post">
                    <input type="hidden" name="id_prod" value="<?= $data['id'] ?>">
                    <input name="qt" type="number" step="0.1">
                    <input type="submit" value="Ajouter au panier">
                </form>
            </div>
        </div>

    <?php } ?>
        <a href="validation.php">Valider mon panier</a>
        On va stocker votre num de commande dans un cookie bisous. &lt;3
    </div>
</body>
</html>