<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>COMMANDES</title>
</head>
<body>
<?php
        require('../pdo.php');

        $req = $pdo->query('select * from commande;');
        $mesInfos = $req->fetchAll();

        foreach($mesInfos as $cmd){
            if($cmd['id_client'] != NULL){
                $req = $pdo->query("select * from client where id=${cmd['id_client']};");
                $client = $req->fetch();
            }else{
                $client = [
                    "id" => '',
                    "nom" => "",
                    "tel" => '',
                    "mail" => ''
                ];
            }
            
    ?>

    <div style="border: 1px solid pink; margin: 3em;">
        <div>
            <span><?= $cmd['id'] ?></span>
            <span><?= $client['nom'] ?></span>
            <span><?= $client['mail'] ?></span>
            <span><?= $client['tel'] ?></span>
        </div>
        <div>
            <?php
            $req = $pdo->prepare('select * from produit_commande where id_commande = ?;');
            $req->execute([$cmd['id']]);
            $mesInfos = $req->fetchAll();

            $total = 0;

            foreach($mesInfos as $data){
                $prod = $pdo->query("select * from produit where id = ${data['id_produit']};")->fetch();
                $total += $prod['prix'] * $data['qt'];
        ?>
            <li>
            <?= $prod['nom'] ?> <?= $prod['prix'] ?>€ <?= $data['qt'] ?>
            </li>

            <?php } ?>
            </ul>
            TOTAL : <?= $total ?>€
            ETAT CMD : <?= $cmd['etat'] ?>
            <?php
                if($cmd['etat'] != 'panier'){
                    ?>
                    <a href="changerEtat.php?id=<?= $cmd['id'] ?>">OK</a>

<?php
                }
            ?>
        </div>
    </div>
    <?php } ?>
</body>
</html>