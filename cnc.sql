drop database if exists cnc;
create database cnc;
use cnc;

create table produit(
    id int auto_increment primary key,
    image varchar(255),
    nom varchar(255),
    prix float,
    dispo boolean
);

insert into produit(image, nom, prix, dispo)
values (
    "https://picsum.photos/200",
    'Patate',
    3.1,
    1
);

insert into produit(image, nom, prix, dispo)
values (
    "https://picsum.photos/200",
    'Tomate',
    999.1,
    0
);

create table commande(
    id int auto_increment primary key,
    id_client int,
    etat enum('panier', 'validée', 'prete', 'collectée') default 'panier'
);

create table produit_commande(
    id_commande int,
    id_produit int,
    qt float,
    primary key (id_commande, id_produit)
);

create table client (
    id int auto_increment primary key,
    mail varchar(255),
    nom varchar(255),
    tel varchar(255)
);

drop user if exists toto@'127.0.0.1';
create user toto@'127.0.0.1' identified by 'salut';
grant all privileges on cnc.* to toto@'127.0.0.1';
